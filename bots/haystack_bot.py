import logging
from haystack.document_stores import InMemoryDocumentStore
from haystack.nodes import BM25Retriever, PDFToTextConverter
from haystack.nodes import FARMReader
from haystack.pipelines import ExtractiveQAPipeline
from pathlib import Path
from botbuilder.core import ActivityHandler
from botbuilder.schema import Activity, ActivityTypes
from botbuilder.core import TurnContext


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class HaystackBot(ActivityHandler):
    def __init__(self):
        logger.info("Initializing HaystackBot...")
        super(HaystackBot, self).__init__()

        self.converter = PDFToTextConverter()
        self.document_store = InMemoryDocumentStore(use_bm25=True)
        self.retriever = BM25Retriever(document_store=self.document_store)
        self.reader = FARMReader(model_name_or_path="timpal0l/mdeberta-v3-base-squad2")
        self.pipe = ExtractiveQAPipeline(reader=self.reader, retriever=self.retriever)

        doc_path = Path('./ww2.pdf')
        self.doc_link = "https://example.com/ww2.pdf"  # Update with your actual source file link

        documents = self.converter.convert(file_path=doc_path, meta={'name': 'ww2'})
        
        for idx, document in enumerate(documents):
            document.id = f"doc_{idx}"
        
        self.document_store.write_documents(documents)
        logger.info("Initialization complete.")

    async def on_message_activity(self, turn_context: TurnContext):
        # Extract the text from the incoming activity
        text = turn_context.activity.text

        # Process the incoming message
        if text.startswith("question:"):
            # Extract the actual question from the text
            question = text[len("question:"):].strip()

            # Run the question through your pipeline
            await self.respond(turn_context, question)

    async def respond(self, turn_context: TurnContext, query):
        logger.info(f"Processing question: {query}")
        prediction = self.pipe.run(
            query=query,
            params={"Retriever": {"top_k": 10}, "Reader": {"top_k": 5}}
        )
        if prediction:
            for idx, answer in enumerate(prediction['answers'], start=1):
                if isinstance(answer, dict):
                    response = f"Answer {idx}:\n\n"
                    response += f"Answer: {answer['answer']}\n\n"
                    response += f"Context: {self.truncate_context(answer['context'])}\n\n"
                    response += f"Source File: {self.doc_link}\n\n"
                    await turn_context.send_activity(response)
                else:
                    await turn_context.send_activity(f"Answer {idx}: {answer}")
                    await turn_context.send_activity(f"Source File: {self.doc_link}")
        else:
            await turn_context.send_activity("Sorry, I couldn't find an answer to your question.")

    def truncate_context(self, context, max_length=200):
        if len(context) > max_length:
            return context[:max_length] + '...'
        return context
